function vendingMachine(email, money, drinkChoice) {
  if (!email && !money) {
    console.log("Please check your input");
  } else if (!email) {
    console.log("Sorry can't process it until your email filled");
  } else if (typeof email != "string") {
    console.log("Invalid input");
  } else if (!money) {
    console.log("Sorry can't process it until you put your money");
  } else if (typeof money != "number") {
    console.log("Invalid input");
  }

  let price;
  switch (drinkChoice) {
    case "mineral water":
      price = 5000;
      break;
    case "cola":
      price = 7500;
      break;
    case "coffee":
      price = 12250;
      break;
    default:
      price = 12250;
      break;
  }

  if (drinkChoice === undefined) {
    drinkChoice = "coffee";
  }

  if (money >= price) {
    let kembalian = money - price;
    console.log(`Welcome ${email} to May's Vending Machine`);
    console.log(`Your choice is ${drinkChoice}, here's your drink`);
    console.log(`Your changes are ${kembalian}`);
    console.log("Thank you");
  } else {
    let kekurangan = money - price;
    console.log(`Welcome ${email} to May's Vending Machine`);
    console.log(
      `Sorry, insufficient balance we can't process your ${drinkChoice}`
    );
    console.log(`You need ${kekurangan} more to buy this drin`);
    console.log("Thank you");
  }
}

vendingMachine("123lynn@server.jp", 25000);
